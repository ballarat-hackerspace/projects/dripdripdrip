
The main two criteria for the pump are:

1) Small
2) Quiet

The plan here is to go to Bunnings and find the smallest, cheapest option. If
that isn't sufficient, research other options, maybe fish tanks?

Looks like this is the good option: https://www.bunnings.com.au/aquapro-ap210-tabletop-water-feature-pump_p2810296

Might need to get a waterproof extension lead protector. Something like this: https://www.bunnings.com.au/ampfibian-x1-weatherproof-connector-protector_p0057700
but maybe less severe