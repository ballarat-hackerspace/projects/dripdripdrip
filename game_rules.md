kDrip, Drip Drip
===


For 2 - 4 players

Each player is a city with its own reservoir system. Each player takes turns, rolling dice and moving around the board.
When they land on a square, they receive that much rain (TODO: units), which goes into their reservoir.

After each player has taken a turn, the round is over. Each player has to use a certain amount of water for cleaning, cooking, manufacturing and other uses.
This water is put back into the water lifecycle, but that player's reservoir is now lower.

Rounds continue. If you land on an action square, draw a card, read the card and perform the action.
Some actions can reduce the water used per round, reducing the amount you need to put into the reservoir when a round ends.

There are five "build" action cards. In each case here, the player who drew the card builds part of the water lifecycle element.

After all five parts have been built, players race to the finish line. However, to win, a player must have a capacity of 80% or more in their own reservoir and reach the Finish tile.


Action Cards
===

### Bonuses
* Educate the community on how to save water
* Receive water from desalination plant
* Rainfall caught in water tanks
* Receive treated waste water
* Recycle grey water
* Retrieve water from underground reservoir
* Implement new water saving strategy eg. in farming
* Enforce water saving restrictions
* Inject funding into improved water infrastructure
 
### Losses

* Water used to fight bushfire
* Unexpected draught incurs loss of water
* Mains burst incurs loss of water
* Population increase
* New Aquatic Centre built
* 
Build x5



Other potential ideas
===

There are so many categories of water use...
