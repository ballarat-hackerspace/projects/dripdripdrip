# dripdripdrip

Alternative name: Waterboard

This repository contains the proof of work for the dripdripdrip project created for GovHack 2019.
See the project page on [the GovHack Hackerspace](https://hackerspace.govhack.org/team_management/teams/665).

TODO Items:

* ~~3D print a cloud~~
* ~~Get a pump and some clear pipe~~
* ~~Get a bucket for water~~
* ~~Laser cut a house~~
* ~~Laser cut a treatment facility~~
* ~~Laser cut some green grass~~
* ~~Laser cut some "ocean" for around the bucket~~
* Print a "map" of the game board, which first needs to be designed. Daniel can colour that in
* Print and cutout the cards, and get the kids to draw / colour in
* Junction boxes / holders for the pipe to ensure they stay in place
* Do the video

Ensure datasets are used to inform the game. List of them is available here: https://hackerspace.govhack.org/challenges/water_from_source_to_tap