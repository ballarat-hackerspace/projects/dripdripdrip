
Water comes from a cloud
===

We'll 3d print a cloud that acts like a shower head and produces "rain" that goes into a bucket.

This looks like the best option on thingiverse: https://www.thingiverse.com/thing:3398062

Water can be pumped in the top, and the existing hole can be used to prop up the whole thing on a pole. 
Not sure of the dimensions to use for the final version - will get a pump first and see about water flow (although a preference to "smaller" )
